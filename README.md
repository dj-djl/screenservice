# Screenservice #

### Intro ###

Screenservice is a quick and easy solution for when a full-on service manager is overkill or maybe you don't have root.
It allows you to run applictions "always" utilising screen

Version: 0.9

### How do I get set up? ###

* git clone https://bitbucket.org/dj-djl/screenservice.git
* cd screenservice
* ./install.sh
* Add scripts into screens folder

### WARNING ###

Do not use this to run web-facing services as yourself!
Instead, create a new locked-down user and install this script as that user.
Then the web-facing service will run as that locked-down user.

Better still, use a proper service manager.

### Licence ###

GNU GPL v3+.

See LICENCE for more info.

### Depdendencies ###
* bash
* screen
