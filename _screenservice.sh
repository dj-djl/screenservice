
# screenservice - crude but effective service management utilising screen
#Copyright (C) 2017 Lee Smith
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>. 


ls ${SCREENSLOCATION} -l  | grep "^...x" | sed -r "s/.*?:[0-9][0-9]\ //"  | sed -r "s/.*?[A-Za-z]* *[0-9]* *[0-9]{4} //"|  grep -v "^~" | grep -v "^#" | ( while read f
do
	f=${SCREENSLOCATION}/$f
	echo $f
	name=AS_$(cat "$f" | grep -i "^[ \t]*#[ \t]*name:" | sed "s/[^:]*:[ \t]*//")
	desc=$(cat "$f" | grep -i "^[ \t]*#[ \t]*desc(ription)?:" | sed "s/[^:]*:[ \t]*//")

	echo -en "Checking $desc ($name) ... "
	if [ $( screen -ls | grep -i "$name" | wc -l)  == 1 ]
	then
		echo "already running"
	else
		echo -en " starting ..."
		screen -c ${SCREENSRC} -dmS "$name" -s "$f" && echo "done" || echo "failed"
	fi
done
)
