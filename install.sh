#!/bin/bash

# very basic installer
#Copyright (C) 2017 Lee Smith
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
										     
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.



LOC=$(pwd)
echo ${LOC}

cat > ./screenservice.sh <(
echo "#!/bin/bash"
echo "SCREENSLOCATION=${LOC}/screens"
echo "SCREENSRC=${LOC}/screenrc #don't use your usual screenrc here. Instead this should be a blank file"
cat _screenservice.sh
)

chmod +x ./screenservice.sh

(crontab -l; echo "* * * * * ${LOC}/screenservice.sh > /dev/null 2>/dev/null") | crontab -
